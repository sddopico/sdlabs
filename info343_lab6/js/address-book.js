/* address-book.js
    this is where you will add your JavaScript to complete Lab 5
*/


$(document).ready( function () {
    getElements();
    render();
});

function getElements() {
    //get the template
    Employees.template = $('#template').html();
    //get the container
    Employees.$container = $('.address-book');
}

function render() {
    //templated_data will be the template string after underscore templates our data
    var templated_data = _.template(Employees.template, Employees);
    //now put it in the container
    Employees.$container.html(templated_data);
}